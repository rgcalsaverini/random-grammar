#include "../include/Lexer.h"
#include <iostream>
#include <utility>

using rndGrammar::Token;
using std::to_string;

vector<Token> rndGrammar::Lexer::lex() {
    line = 0;
    src_col = 0;
    current = 0;

    while (!isSourceFinished()) {
        src_start = current;
        scanToken();
    }

    if (tokens.empty()) parseError("File appears to be empty");

    while(tokens[0].type== tokenType::NEW_LINE) tokens.erase(tokens.begin());
    while(tokens.back().type== tokenType::NEW_LINE) tokens.pop_back();

    addToken(END_OF_FILE);

    return tokens;
}

bool rndGrammar::Lexer::isSourceFinished() {
    return current >= source.length();
}

void rndGrammar::Lexer::addToken(rndGrammar::tokenType type, string text) {
    Token token;
    token.type = type;
    token.value = std::move(text);
    token.line = line;
    token.column = src_col;
    tokens.push_back(token);
}

void rndGrammar::Lexer::scanToken() {
    char c = advanceSrc();
    if (isSpace(c)) return;
    else if (c == '\n') scanNewLine();
    else if (c == '@') addToken(tokenType::AT);
    else if (c == '+') addToken(tokenType::PLUS);
    else if (c == ':') addToken(tokenType::COLON);
    else if (c == '(') addToken(tokenType::LEFT_PAREN);
    else if (c == ')') addToken(tokenType::RIGHT_PAREN);
    else if (c == '{') addToken(tokenType::LEFT_CURLY);
    else if (c == '}') addToken(tokenType::RIGHT_CURLY);
    else if (c == '#') scanComment();
    else if (isQuote(c)) scanString(c);
    else if (isDigit(c)) scanInteger();
    else if (isAlphaOrUnderscore(c)) scanIdentifier();
    else errorUnexpected("character " + string({'"', c, '"'}));
}

void rndGrammar::Lexer::scanComment() {
    while (peek() != '\n' && !isSourceFinished()) advanceSrc();
}


char rndGrammar::Lexer::advanceSrc(size_t num) {
    current += num;
    src_col += num;
    return source[current - 1];
}

void rndGrammar::Lexer::newLine() {
    src_col = 0;
    line++;
}

char rndGrammar::Lexer::peek() {
    if (isSourceFinished()) return '\0';
    return source[current];
}

char rndGrammar::Lexer::peekNext() {
    if (current + 1 >= source.length()) return '\0';
    return source[current + 1];
}

void rndGrammar::Lexer::scanString(char quote_char) {
    while (true) {
        if (isSourceFinished()) {
            errorUnclosed("string1");
            break;
        }
        if (peek() == '\n') {
            errorUnclosed("string2");
            break;
        }
        if (peek() == '\\' && peekNext() == quote_char) {
            advanceSrc();
        } else if (peek() == quote_char) {
            advanceSrc();
            break;
        }
        advanceSrc();
    }
    addToken(tokenType::STRING, src_start + 1, current - 1);
}

void rndGrammar::Lexer::scanIdentifier() {
    while (isIdChar(peek()) && !isSourceFinished()) advanceSrc();
    addToken(tokenType::IDENTIFIER);
}


void rndGrammar::Lexer::scanInteger() {
    while (isDigit(peek()) && !isSourceFinished()) advanceSrc();
    if (peek() != ' ' && peek() != ')' && peek() != '}') {
        errorUnexpected(string({'"', peek(), '"'}), "after integer");
    }
    addToken(tokenType::INT);
}

void rndGrammar::Lexer::parseError(const string &message) {
    throw std::runtime_error(message.c_str());
}

void rndGrammar::Lexer::parseError(const string &message, const rndGrammar::Token &token) {
    parseError(message, token.line, token.column);
}

void rndGrammar::Lexer::parseError(const string &message, size_t line, size_t col) {
    parseError("@" + to_string(line + 1) + ":" + to_string(col + 1) + " " + message);
}

void rndGrammar::Lexer::errorUnclosed(const string &what) {
    parseError("Unclosed " + what, line, src_col);
}

void rndGrammar::Lexer::errorUnexpected(const string &what, const string &where) {
    if (where.length()) {
        parseError("Unexpected " + what + " " + where, line, src_col);
    } else {
        parseError("Unexpected " + what, line, src_col);
    }
}

void rndGrammar::Lexer::debugPrintTokens() {
    map<tokenType, string> token_type_map = {
            {tokenType::PLUS,        "\033[38;2;200;0;0mPLUS\033[0m       "},
            {tokenType::STRING,      "\033[38;2;0;200;0mSTRING\033[0m     "},
            {tokenType::LEFT_PAREN,  "\033[38;2;0;100;200mLEFT_PAREN\033[0m "},
            {tokenType::RIGHT_PAREN, "\033[38;2;0;100;200mRIGHT_PAREN\033[0m"},
            {tokenType::COLON,       "\033[38;2;0;160;160mCOLON\033[0m      "},
            {tokenType::INT,         "\033[38;2;200;100;200mINT\033[0m        "},
            {tokenType::IDENTIFIER,  "\033[38;2;200;180;20mID\033[0m         "},
            {tokenType::END_OF_FILE, "EOF\033[0m        "},
            {tokenType::NEW_LINE, "NL         "},
    };

    for (const auto &token : tokens) {
        string type_str = token_type_map.at(token.type);
        string value;
        if(token.type == tokenType::STRING) {
            value = "\033[38;2;100;100;100m = '\033[0m" + token.value + "\033[38;2;100;100;100m'\033[0m";
        } else if(token.type == tokenType::IDENTIFIER || token.type == tokenType::INT){
            value = "\033[38;2;100;100;100m = \033[0m" + token.value + "";
        }
        std::cout << type_str << " \033[38;2;100;100;100m(" << token.line << ":" << token.column << ")\033[0m" << value << std::endl;
    }

}


void rndGrammar::Lexer::scanNewLine() {
    newLine();
    while(peek() == '\n') {
        newLine();
        advanceSrc();
    }
    addToken(tokenType::NEW_LINE);
}

