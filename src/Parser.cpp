#include <iostream>
#include <fstream>
#include <streambuf>
#include "../include/Parser.h"

using rndGrammar::Generator;

using std::string;
using std::ifstream;

rndGrammar::Parser::Parser(const string &src_or_file, bool is_file) :
        lexer(is_file ? loadFile(src_or_file) : src_or_file),
        current(0) {}

string rndGrammar::Parser::loadFile(const string& filename) {
    ifstream file_stream(filename);
    string str;

    file_stream.seekg(0, std::ios::end);
    str.reserve(file_stream.tellg());
    file_stream.seekg(0, std::ios::beg);
    str.assign((std::istreambuf_iterator<char>(file_stream)),
               std::istreambuf_iterator<char>());
    return str;
}

void rndGrammar::Parser::importDependency(Generator &generator) {
    string imported_entry = consume(tokenType::IDENTIFIER, err_msg_import_id).value;
    consumeKeyword("from");
    string filename = consume(tokenType::STRING, err_msg_import_filename).value;
//    Parser dep_parser(filename, true, imported_entry);
//    Generator importedGen = dep_parser.parse();
//    importedGen.imported_as()
}

Generator rndGrammar::Parser::parse() {
    tokens = lexer.lex();
    current = 0;
    Generator generator;

    parseHeader(generator);

    while (!isFinished()) {
        discardNewLines();
        string rule_id = consume(tokenType::IDENTIFIER, err_msg_rule_id).value;
        consume(tokenType::COLON, err_msg_colon);
        consume(tokenType::NEW_LINE, err_msg_new_line);
        generator.addRule(rule_id);
        parseAlternative(rule_id, generator);
    }
    return generator;
}

void rndGrammar::Parser::parseHeader(Generator &generator) {
    while (match(tokenType::AT)) {
        string identifier = consume(tokenType::IDENTIFIER, err_msg_header_id).value;
        if (identifier == "entry") {
            generator.entry_rule = consume({tokenType::IDENTIFIER, tokenType::STRING}, err_msg_no_entry_id).value;
        } else if(identifier == "import") {
            importDependency(generator);
        } else {
            Lexer::parseError(err_msg_unknown_header + identifier + "'", peek());
        }
        consume(tokenType::NEW_LINE, err_msg_expect_new_line);
    }
}

void rndGrammar::Parser::parseAlternative(const string &rule_id, Generator &generator) {
    bool previous_was_non_trivial_alternative = false;

    while (!isFinished() && !matchNewRule()) {
        if (previous_was_non_trivial_alternative && previous().type != tokenType::NEW_LINE) {
            Lexer::parseError(err_msg_concat_multiline, peek());
        }
        RuleAlternative alternative = RuleAlternative();
        alternative.weight = 100;

        if (match(tokenType::LEFT_PAREN)) {
            string value = consume(tokenType::INT, err_msg_weight).value;
            alternative.weight = std::stoul(value);
            consume(tokenType::RIGHT_PAREN, err_msg_close_par);
        }
        do {
            RuleAlternativeItem item = RuleAlternativeItem();

            Token id_or_str = consume({tokenType::IDENTIFIER, tokenType::STRING}, err_msg_alternative);
            item.value = id_or_str.value;
            item.isLiteral = id_or_str.type == tokenType::STRING;

            if (match(tokenType::LEFT_CURLY)) {
                item.canRepeat = true;
                item.repeatThresh = std::stoul(consume(tokenType::INT, err_msg_thresh_r_item).value);
                item.repeatRMax = std::stoul(consume(tokenType::INT, err_msg_max_r_item).value);
                consume(tokenType::RIGHT_CURLY, err_msg_right_curly);
            } else {
                item.canRepeat = false;
            }
            alternative.items.push_back(item);
        } while (match(tokenType::PLUS) && !matchNewRule());
        previous_was_non_trivial_alternative = alternative.items.size() > 1;
        generator.addAlternative(rule_id, alternative);
    }
}

bool rndGrammar::Parser::match(const vector<tokenType> &token_vector) {
    for (auto it : token_vector) {
        if (match(it)) return true;
    }
    return false;
}

bool rndGrammar::Parser::match(const rndGrammar::tokenType &token) {
    if (check(token)) {
        advance();
        return true;
    }
    return false;
}


bool rndGrammar::Parser::matchNewRule() {
    discardNewLines();
    size_t start = current;
    bool is_new_rule = match(tokenType::IDENTIFIER) && match(tokenType::COLON) && match(tokenType::NEW_LINE);
    current = start;
    return is_new_rule;
}

const rndGrammar::Token &rndGrammar::Parser::consume(rndGrammar::tokenType type, const string &message) {
    if (check(type)) return advance();
    Lexer::parseError(message, peek());
    throw;
}

const rndGrammar::Token &rndGrammar::Parser::consume(const vector<tokenType> &types, const string &message) {
    for (auto const &it : types) {
        if (check(it)) {
            return advance();
        }
    }
    Lexer::parseError(message, peek());
    throw;
}

bool rndGrammar::Parser::check(rndGrammar::tokenType token) {
    if (isFinished()) return false;
    return peek().type == token;
}

const rndGrammar::Token &rndGrammar::Parser::advance() {
    if (!isFinished()) current++;
    previous();
    return previous();
}

const rndGrammar::Token &rndGrammar::Parser::previous() {
    return tokens[current - 1];
}

const rndGrammar::Token &rndGrammar::Parser::peek() {
    return tokens[current];
}

bool rndGrammar::Parser::isFinished() {
    return tokens[current].type == tokenType::END_OF_FILE || current >= tokens.size();
}

void rndGrammar::Parser::consumeKeyword(const string &kw) {
    string message = "Expecting keyword '" + kw + "'";
    string consumed_id = consume(tokenType::IDENTIFIER, message).value;
    if (consumed_id != kw) {
        message += " and instead got '" + consumed_id + "'";
        Lexer::parseError(message, peek());
    }
}



