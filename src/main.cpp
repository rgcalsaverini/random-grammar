#include <iostream>
#include <string>
#include "../include/Parser.h"
#include "../include/Generator.h"

using rndGrammar::Parser;
using rndGrammar::Generator;

int main(int argc, char **argv) {
    int count = 1;
    if (argc < 2 || argc > 4) {
        std::cout << "Usage: rnd FILE [COUNT]\n";
        return 1;
    } else if (argc == 3) {
        count = std::atoi(argv[2]);
    }

    Parser par(argv[1]);
    Generator gen = par.parse();
//    gen.debugPrintRules();
    for (int i = 0; i < count; i++) {
        std::cout << gen.generate() << "\n";
    }

    return 0;
}