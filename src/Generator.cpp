#include <iostream>

#include "../include/Generator.h"

void rndGrammar::Generator::addRule(const string& name) {
    vector<RuleAlternative> empty = {};
    rules.insert({name, empty});
    total_weights.insert({name, 0});
}

void rndGrammar::Generator::addAlternative(const string &rule, const rndGrammar::RuleAlternative& alternative) {
    if(!rules.count(rule)) {
        throw std::runtime_error("Missing rule '" + rule + "'");
    }
    rules.at(rule).push_back(alternative);
    total_weights.at(rule) += alternative.weight;
}

void rndGrammar::Generator::debugPrintRules() {
    for(const auto &rule : rules) {
        std::cout << rule.first <<  "  [" <<(total_weights.at(rule.first)) << "]\n";
        for(const auto &alternative: rule.second) {
            std::cout << "    " << alternative.weight << ") ";
            for(const auto &item: alternative.items) {
                if(item.isLiteral) {
                    std::cout << "'" << item.value << "' ";
                } else {
                    std::cout << item.value << " ";
                }
            }
            std::cout << "\n";
        }
    }
}

string rndGrammar::Generator::generate() {
    std::random_device random_device;
    random_engine = std::mt19937(random_device());
    return expand(entry_rule);
}

string rndGrammar::Generator::expand(string rule_id) {
    string res;
    if(!rules.count(rule_id)) {
        throw std::runtime_error("Missing rule '" + rule_id + "'");
    }
    RuleAlternative alternative = pickRandomAlternative(rule_id);
    for(auto const &item: alternative.items) {
        res += expandItem(item);
    }

    return res;
}

string rndGrammar::Generator::expandItem(const rndGrammar::RuleAlternativeItem& item) {
    if(!item.isLiteral) {
        return expand(item.value);
    }
    if(!item.canRepeat) {
        return item.value;
    }
    string res;
    std::uniform_int_distribution<unsigned long> dist(0, item.repeatRMax);
    while(dist(random_engine) <= item.repeatThresh) {
        res += item.value;
    }
    return res;
}

rndGrammar::RuleAlternative rndGrammar::Generator::pickRandomAlternative(const string& rule_id) {
    std::uniform_int_distribution<unsigned long> dist(0, total_weights.at(rule_id));
    unsigned long random_w = dist(random_engine);
    for (auto const &alternative: rules.at(rule_id)) {
        if(alternative.weight >= random_w) return alternative;
        random_w -= alternative.weight;
    }
    throw std::runtime_error("Failed to pick alternative for rule '" + rule_id + "'");
}


