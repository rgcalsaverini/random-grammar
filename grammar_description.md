The EBNF notation of the grammar is as follows:
```
FILE            ::= HEADER? RULE+
HEADER          ::= ( HEADER_ITEM '\n' )+
HEADER_ITEM     ::= '@' IDENTIFIER (' ' IDENTIFIER)*
RULE            ::= IDENTIFIER ':' '\n' ( PATH "\n")+
PATH            ::= ('(' INT ')')? ( CONCAT_PATH | NON_CONCAT_PATH )
CONCAT_PATH     ::= ( STRING | IDENTIFIER ) ( '+' ( STRING | IDENTIFIER ) )+
NON_CONCAT_PATH ::= ( STRING | IDENTIFIER ) ( ' ' ( STRING | IDENTIFIER ) )*
STRING          ::= '"' [^"]* '"'
IDENTIFIER      ::= [a-zA-Z_] [a-zA-Z0-9_]*
INT             ::= [0-9]+
```

You can check out the diagram by pasting this [here](https://www.bottlecaps.de/rr/ui)