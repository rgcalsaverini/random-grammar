#ifndef RND_GRAMMAR_PARSER_H
#define RND_GRAMMAR_PARSER_H

#include <string>
#include <vector>
#include <map>
#include "Lexer.h"
#include "Generator.h"

using std::pair;
using std::string;
using std::vector;
using std::map;

namespace rndGrammar {
    class Parser {
    public:
        Parser(const string& src_or_file, bool is_file=true);

        Generator parse();

    private:
        static string loadFile(const string& filename);

        void importDependency(Generator& generator);

        void parseAlternative(const string &rule_id, Generator& generator);

        void parseHeader(Generator& generator);

        bool match(const vector<tokenType> &token_vector);

        bool match(const tokenType &token);

        bool matchNewRule();

        const Token &consume(tokenType type, const string &message);

        const Token &consume(const vector<tokenType> &types, const string &message);

        void consumeKeyword(const string &kw);

        bool check(tokenType token);

        const Token &advance();

        const Token &previous();

        const Token &peek();

        bool isFinished();

        inline void discardNewLines() { while (match(tokenType::NEW_LINE)); }

    private:
        vector<Token> tokens;
        size_t current;
        Lexer lexer;
        string imported_as;

        const string err_msg_rule_id = "Expecting rule identifier";
        const string err_msg_colon = "Expecting colon after rule identifier";
        const string err_msg_new_line = "Expecting new line after rule definition";
        const string err_msg_weight = "Expecting rule alternative weight value after opening parenthesis";
        const string err_msg_close_par = "Expecting closing parenthesis after weight";
        const string err_msg_alternative = "Expecting rule alternative";
        const string err_msg_thresh_r_item = "Expecting threshold for repetition clause";
        const string err_msg_max_r_item = "Expecting max for repetition cluse";
        const string err_msg_right_curly = "Expecting closing curly braces";
        const string err_msg_concat_multiline = "When concatenating items, only one alternative per line is allowed";
        const string err_msg_header_id = "Expecting identifier after '@'";
        const string err_msg_unknown_header = "Unknown header directive '";
        const string err_msg_no_entry_id = "Expecting entry rule id";
        const string err_msg_expect_new_line = "Expecting line break after expression";
        const string err_msg_import_id = "Expecting imported root rule name";
        const string err_msg_import_filename = "Expecting imported file path";
//        const string err_msg_ = "";
    };
}

#endif //RND_GRAMMAR_PARSER_H
