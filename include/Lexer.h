#ifndef RND_GRAMMAR_LEXER_H
#define RND_GRAMMAR_LEXER_H

#include <string>
#include <vector>
#include <map>

using std::pair;
using std::string;
using std::vector;
using std::map;

namespace rndGrammar {
    enum tokenType {
        PLUS,
        STRING,
        LEFT_PAREN,
        RIGHT_PAREN,
        LEFT_CURLY,
        RIGHT_CURLY,
        COLON,
        INT,
        IDENTIFIER,
        END_OF_FILE,
        NEW_LINE,
        AT,
    };

    struct Token {
        tokenType type;
        string value;
        size_t line;
        size_t column;
    };

    class Lexer {
    public:
        Lexer(string src) : source(src) {};

        vector<Token> lex();

        void debugPrintTokens();

        static void parseError(const string& message, const Token& token);

    private:
        inline void newLine();

        bool isSourceFinished();

        inline void scanToken();

        inline void scanNewLine();

        inline void scanComment();

        inline void scanInteger();

        inline void scanString(char quote_char);

        void scanIdentifier();

        inline char advanceSrc(size_t num= 1);

        inline char peek();

        inline char peekNext();

        void addToken(tokenType type) { return addToken(type, src_start, current); }

        void addToken(tokenType type, size_t s, size_t e) { addToken(type, source.substr(s, e - s)); }

        void addToken(tokenType type, string text);

        inline static bool isSpace(char c) { return c == ' ' || c == '\t' || c == '\r'; }

        inline static bool isQuote(char c) { return c == '\'' || c == '"'; }

        inline static bool isDigit(char c) { return c >= '0' && c <= '9'; }

        inline static bool isAlphaOrUnderscore(char c) { return (c >= 'a' && c <= 'z') || ( c >= 'A' && c <= 'Z') || c == '_'; }

        inline static bool isIdChar(char c) { return isAlphaOrUnderscore(c) || isDigit(c); }

        static void parseError(const string& message);

        static void parseError(const string& message, size_t line, size_t col);

        void errorUnclosed(const string& what);

        void errorUnexpected(const string& what, const string& where = "");


    public:
        vector<Token> tokens;

    private:
        size_t line;
        size_t src_col;
        size_t src_start;
        size_t current;
        string source;
    };
}


#endif //RND_GRAMMAR_LEXER_H
