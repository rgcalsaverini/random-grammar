#ifndef RND_GRAMMAR_GENERATOR_H
#define RND_GRAMMAR_GENERATOR_H

#include <map>
#include <string>
#include <vector>
#include <random>

using std::map;
using std::string;
using std::vector;

namespace rndGrammar {
    struct RuleAlternativeItem {
        bool isLiteral;
        bool canRepeat = false;
        unsigned long repeatThresh = 0;
        unsigned long repeatRMax = 0;
        string value;
    };

    struct RuleAlternative {
        vector<RuleAlternativeItem> items;
        unsigned long weight;
    };

    class Generator {
    public:
        void addRule(const string &name);

        void addAlternative(const string &rule, const RuleAlternative &alternative);

        void debugPrintRules();

        string generate();

    private:
        string expand(string rule_id);

        string expandItem(const RuleAlternativeItem&);

        RuleAlternative pickRandomAlternative(const string& rule_id);

    public:
        string entry_rule = "ROOT";
    private:
        map<string, vector<RuleAlternative>> rules;
        map<string, unsigned long> total_weights;
        std::mt19937 random_engine;
    };
}


#endif //RND_GRAMMAR_GENERATOR_H
