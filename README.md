
# RND-Grammar

This is an utility to generate random words or short simple phrases, to help with generative text contents on
games. 


## How to use

This can be used as a library, or you can directly use the command-line utility to pre-generate text.

To compile it, run:

```
cmake .
make
```

and you can run it like this: `./rnd_grammar examples/fairy_tale_beginning 1`

### The language file

A language file is comprised of **formation rules** and **literal strings**.


```yaml
# A rule has a name and one or more possible alternatives. 
# Each alternative is on a line after the rule, and can be another rule or, literal string or any combination of that.
# Things can be concatenated with the + sign
# The entry rule is called ROOT by default, but that can be changed
ROOT:
  FRUIT
  "I am having" + CANDY
  "I'm not hungry"

# If your rule is a terminal, meaning it has only literal strings, you can write them all in one line
FRUIT:
    "apple" "banana" "pear" "tomato"

# And you can chain rules as you please.
# Numbers on brackets before the alternative change the weight of that path
CANDY:
    (2) CANDY_NAME
    (1) CANDY_NAME + " and then a " + CANDY_NAME

CANDY_NAME:
    "chocolate" "cake" "pie" "cheesecake"
```

This file will generate stuff like:
```
apple
I am having pie
tomato
I am having cheesecake and then a chocolate
I'm not hungry
banana
I am having chocolate
...
```

You can also use recursion. For example the following:

```
ROOT:
   (1) VOCALIZATION + "!"
   (10)  VOCALIZATION + " " + ROOT

VOCALIZATION:
    "badum" "tum tum" "tum" "hum" "ba" "bi dum" "ba dum" "dum"
```

generates things like:

```
badum ba ba dum hum tum badum ba dum tum tum tum tum!
bi dum ba!
ba dum!
tum tum!
bi dum bi dum bi dum hum ba dum!
```




## Next steps


Imports will inject the rules from another source into this one, rename the ROOT rule into what we choose as the
top level name for that import, and prefix all the other supporting rules with `@<TOP_LEVEL_NAME>_<RULE_ID>`:

```
@import SCOPED_NAME from 'some/dep'
```
------------------------------------------------

